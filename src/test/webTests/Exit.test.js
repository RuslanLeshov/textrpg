import Vue from 'vue'
import Vuetify from 'vuetify'
import VueResource from 'vue-resource'
import Exit from "../../main/resources/static/js/components/Exit.vue";
import {mount} from "@vue/test-utils";
import StartPage from "../../main/resources/static/js/pages/StartPage";

Vue.use(VueResource);
Vue.use(Vuetify, {iconfont: 'mdiSvg'});


describe('Exit', () => {
    test('Проверка html', () => {
        const wrapper = mount(Exit);
        expect(wrapper.html()).toContain("<div class=\"row justify-center\">");
        expect(wrapper.html()).toContain("<div role=\"dialog\" class=\"v-dialog__container\">");
        expect(wrapper.html()).toContain("<div tabindex=\"0\" class=\"v-list-item v-list-item--link theme--light\">");
        expect(wrapper.html()).toContain("<div class=\"v-list-item__action\">");
        expect(wrapper.html()).toContain("<i aria-hidden=\"true\" class=\"v-icon notranslate mdi mdi-home theme--light\">");
        expect(wrapper.html()).toContain("<div class=\"v-list-item__content\">");
        expect(wrapper.html()).toContain("<div class=\"v-list-item__title\">");
        expect(wrapper.html()).toContain("Выход");
    });
    test('Проверка начальных значений', () => {
        const wrapper = mount(StartPage);
        expect(wrapper.vm.dialog).toBe(false);
    });
});