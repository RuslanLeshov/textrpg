import Vue from 'vue'
import Vuetify from 'vuetify'
import VueResource from 'vue-resource'
import Logs from "../../main/resources/static/js/components/Logs.vue";
import LogLine from "../../main/resources/static/js/components/LogLine.vue";

import {mount} from "@vue/test-utils";


Vue.use(VueResource);
Vue.use(Vuetify, {iconfont: 'mdiSvg'});

describe('Logs', () => {
    test('Проверка html', () => {
        const wrapper = mount(Logs);
        expect(wrapper.html()).toContain("<div role=\"list\" class=\"v-list v-sheet v-sheet--tile theme--light\">");
    });
    test('Проверка начальных значений', () => {
        const wrapper = mount(Logs);
        expect(wrapper.vm.lines).toStrictEqual([]);
    });
});

describe('LogLine', () => {
    test('Проверка html', () => {
        const wrapper = mount(LogLine);
        expect(wrapper.html()).toContain("<div tabindex=\"-1\" class=\"v-list-item theme--light\">");
        expect(wrapper.html()).toContain("<div class=\"v-list-item__content\">");
        expect(wrapper.html()).toContain("<div class=\"v-list-item__title text-left\">");
    });
});