import Vue from 'vue'
import Vuetify from 'vuetify'
import VueResource from 'vue-resource'
import {mount} from "@vue/test-utils";
import Inventory from "../../main/resources/static/js/components/Inventory.vue";

Vue.use(VueResource);
Vue.use(Vuetify, {iconfont: 'mdiSvg'});

describe('Inventory', () => {
    test('Проверка html', () => {
        const wrapper = mount(Inventory);
        expect(wrapper.html()).toContain("<div class=\"row justify-center\">");
        expect(wrapper.html()).toContain("<div role=\"dialog\" class=\"v-dialog__container\">");
        expect(wrapper.html()).toContain("<div tabindex=\"0\" class=\"v-list-item v-list-item--link theme--light\">");
        expect(wrapper.html()).toContain("<div class=\"v-list-item__action\">");
        expect(wrapper.html()).toContain("<i aria-hidden=\"true\" class=\"v-icon notranslate mdi mdi-bag-personal theme--light\">");
        expect(wrapper.html()).toContain("<div class=\"v-list-item__content\">");
        expect(wrapper.html()).toContain("<div class=\"v-list-item__title\"");
        expect(wrapper.html()).toContain("Инвентарь");
    });
    test('Проверка начальных значений', () => {
        const wrapper = mount(Inventory);
        expect(wrapper.vm.dialog).toBe(false);
    });
});