import Vue from 'vue'
import Vuetify from 'vuetify'
import VueResource from 'vue-resource'
import StartPage from "../../main/resources/static/js/pages/StartPage.vue";
import {mount} from "@vue/test-utils";

Vue.use(VueResource);
Vue.use(Vuetify, {iconfont: 'mdiSvg'});


describe('StartPage', () => {
    test('Проверка html', () => {
        const wrapper = mount(StartPage);
        expect(wrapper.html()).toContain("<div class=\"background\">");
        expect(wrapper.html()).toContain("<div role=\"dialog\" class=\"v-dialog__container\">");
        expect(wrapper.html()).toContain("<button type=\"button\" class=\"profileButton v-btn v-btn--contained theme--dark v-size--default green\" id=\"10\">");
        expect(wrapper.html()).toContain("<span class=\"v-btn__content\">");
        expect(wrapper.html()).toContain("Заполнить анкету");
    });
    test('Проверка начальных значений', () => {
        const wrapper = mount(StartPage);
        expect(wrapper.vm.dialog).toBe(false);
        expect(wrapper.vm.name).toBe("");
        expect(wrapper.vm.freePoints).toBe(10);
        expect(wrapper.vm.player).toBe(null);
        expect(wrapper.vm.attributes[0].value).toBe(0);
        expect(wrapper.vm.attributes[0].name).toBe("HP");
        expect(wrapper.vm.attributes[1].value).toBe(0);
        expect(wrapper.vm.attributes[1].name).toBe("POWER");
        expect(wrapper.vm.power).toBe(0);
        expect(wrapper.vm.hp).toBe(0);
    });
    test('Проверка charUp', () => {
        const wrapper = mount(StartPage);
        wrapper.vm.charUp(0);
        expect(wrapper.vm.freePoints).toBe(9);
        wrapper.vm.charUp(1);
        expect(wrapper.vm.freePoints).toBe(8);
        wrapper.vm.freePoints = 0;
        wrapper.vm.charUp(1);
        expect(wrapper.vm.freePoints).toBe(0);
    });
    test('Проверка charDown', () => {
        const wrapper = mount(StartPage);
        wrapper.vm.attributes[0].value = 1;
        wrapper.vm.attributes[1].value = 1;
        wrapper.vm.charDown(0);
        expect(wrapper.vm.freePoints).toBe(10);
        wrapper.vm.charDown(1);
        expect(wrapper.vm.freePoints).toBe(10);
        wrapper.vm.attributes[0].value = 10;
        wrapper.vm.attributes[1].value = 10;
        wrapper.vm.charDown(0);
        expect(wrapper.vm.freePoints).toBe(11);
        wrapper.vm.charDown(1);
        expect(wrapper.vm.freePoints).toBe(12);
    });
    test('Проверка setPlayer', () => {
        const wrapper = mount(StartPage);
        wrapper.vm.setPlayer();
        expect(wrapper.vm.name).toBe("");
    })
});

