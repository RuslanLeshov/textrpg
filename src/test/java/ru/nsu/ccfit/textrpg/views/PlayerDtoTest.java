package ru.nsu.ccfit.textrpg.views;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.nsu.ccfit.textrpg.TextRpgApplicationTests;
import ru.nsu.ccfit.textrpg.domain.PlayerState;

import static org.junit.jupiter.api.Assertions.assertEquals;


class PlayerDtoTest extends TextRpgApplicationTests {
    private PlayerDto playerDto;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        playerDto = new PlayerDto();
    }

    @Test
    public void testHp() {
        playerDto.setHp(100);
        assertEquals(100, playerDto.getHp());
    }

    @Test
    public void testName() {
        playerDto.setName("Name");
        assertEquals("Name", playerDto.getName());
    }

    @Test
    public void testId() {
        playerDto.setId(1L);
        assertEquals(1L, playerDto.getId());
    }

    @Test
    public void testPower() {
        playerDto.setPower(30);
        assertEquals(30, playerDto.getPower());
    }

    @Test
    public void testPlayerStateConstructor() {
        PlayerState playerState = new PlayerState(playerRepository.findById(4L).get());
        PlayerDto playerDto = new PlayerDto(playerState);

        assertEquals(4L, playerDto.getId());
        assertEquals(100, playerDto.getHp());
        assertEquals("Марио", playerDto.getName());
        assertEquals(25, playerDto.getPower());
    }
}