package ru.nsu.ccfit.textrpg.views;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.nsu.ccfit.textrpg.TextRpgApplicationTests;
import ru.nsu.ccfit.textrpg.domain.EnemyState;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EnemyDtoTest extends TextRpgApplicationTests {
    private EnemyDto enemyDto;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        enemyDto = new EnemyDto();
    }

    @Test
    public void testHp() {
        enemyDto.setHp(30);
        assertEquals(30, enemyDto.getHp());
    }

    @Test
    public void testName() {
        enemyDto.setName("Name");
        assertEquals("Name", enemyDto.getName());
    }

    @Test
    public void testId() {
        enemyDto.setId(1L);
        assertEquals(1L, enemyDto.getId());
    }

    @Test
    public void testEnemyStateConstructor() {
        EnemyState enemyState = new EnemyState(enemyRepository.findById(1L).get());
        EnemyDto enemyDto = new EnemyDto(enemyState);

        assertEquals(1L, enemyDto.getId());
        assertEquals(30, enemyDto.getHp());
        assertEquals("Дикий кабан", enemyDto.getName());
    }
}
