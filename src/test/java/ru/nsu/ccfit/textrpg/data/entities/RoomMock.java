package ru.nsu.ccfit.textrpg.data.entities;

import java.util.ArrayList;

public class RoomMock {
    private Room room;

    public RoomMock() {
        room = new Room();
        room.setId(1L);

        ArrayList<Enemy> enemies = new ArrayList<>();
        enemies.add(new EnemyMock(1L).getEnemy());
        enemies.add(new EnemyMock(2L).getEnemy());
        enemies.add(new EnemyMock(3L).getEnemy());
        room.setEnemies(enemies);

        ArrayList<TextMessage> before = new ArrayList<>();
        before.add(new TextMessageMock(1L).getMsg());
        before.add(new TextMessageMock(2L).getMsg());
        before.add(new TextMessageMock(3L).getMsg());

        ArrayList<TextMessage> after = new ArrayList<>();
        after.add(new TextMessageMock(4L).getMsg());
        after.add(new TextMessageMock(5L).getMsg());
        after.add(new TextMessageMock(6L).getMsg());

        room.setMessagesBeforeBattle(before);
        room.setMessagesAfterBattle(after);
    }

    public Room getRoom() {
        return room;
    }
}
