package ru.nsu.ccfit.textrpg.data.entities;

import java.util.ArrayList;
import java.util.HashMap;

public class EnemyMock {
    private Enemy enemy;

    public EnemyMock(long id) {
        enemy = new Enemy();
        enemy.setId(id);
        HashMap<String, Stat> stats = new HashMap<>();
        Stat hp = new Stat();
        Stat power = new Stat();

        if (id == 1L) {
            enemy.setName("Дикий кабан");

            hp.setId(1L);
            hp.setMinValue(0);
            hp.setMaxValue(-1);
            hp.setBaseValue(30);

            power.setId(2L);
            power.setMinValue(-1);
            power.setMaxValue(-1);
            power.setBaseValue(5);
        } else if (id == 2L) {
            enemy.setName("Каменный страж");

            hp.setId(3L);
            hp.setMinValue(0);
            hp.setMaxValue(-1);
            hp.setBaseValue(50);

            power.setId(4L);
            power.setMinValue(-1);
            power.setMaxValue(-1);
            power.setBaseValue(10);
        } else if (id == 3L) {
            enemy.setName("Гоблин из подземелья");

            hp.setId(5L);
            hp.setMinValue(0);
            hp.setMaxValue(-1);
            hp.setBaseValue(40);

            power.setId(6L);
            power.setMinValue(-1);
            power.setMaxValue(-1);
            power.setBaseValue(7);
        }
        hp.setModifiers(new ArrayList<>());
        power.setModifiers(new ArrayList<>());

        stats.put("hp", hp);
        stats.put("power", power);

        enemy.setStats(stats);
    }

    public Enemy getEnemy() {
        return enemy;
    }
}
