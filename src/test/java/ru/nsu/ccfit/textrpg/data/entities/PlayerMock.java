package ru.nsu.ccfit.textrpg.data.entities;

import java.util.ArrayList;
import java.util.HashMap;

public class PlayerMock {
    private Player player;

    public PlayerMock(Long id) {
        player = new Player();
        if (id == 4L) {
            player.setId(id);
            player.setName("Марио");

            HashMap<String, Stat> stats = new HashMap<>();
            Stat hp = new Stat();
            hp.setBaseValue(100);
            hp.setMinValue(0);
            hp.setMaxValue(-1);
            hp.setId(7L);
            hp.setModifiers(new ArrayList<>());
            Stat power = new Stat();
            power.setBaseValue(25);
            power.setMinValue(-1);
            power.setMaxValue(-1);
            power.setId(8L);
            power.setModifiers(new ArrayList<>());

            stats.put("hp", hp);
            stats.put("power", power);
            player.setStats(stats);
        }
    }

    public Player getPlayer() {
        return player;
    }
}
