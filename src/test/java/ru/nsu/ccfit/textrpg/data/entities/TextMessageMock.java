package ru.nsu.ccfit.textrpg.data.entities;

public class TextMessageMock {
    private TextMessage msg;
    public TextMessageMock(long id) {
        msg = new TextMessage();
        msg.setId(id);
        switch ((int)id) {
            case 1:
                msg.setText("Приветствуем тебя в тестовой комнате");
                break;
            case 2:
                msg.setText("Перед тобой стоит 3 противника");
                break;
            case 3:
                msg.setText("Тебе нужно сразиться с ними");
                break;
            case 4:
                msg.setText("Первая победа!");
                break;
            case 5:
                msg.setText("Поздравляю. Теперь ты готов к новым испытаниям");
                break;
            case 6:
                msg.setText("Удачи, герой");
                break;
        }
    }

    public TextMessage getMsg() {
        return msg;
    }
}
