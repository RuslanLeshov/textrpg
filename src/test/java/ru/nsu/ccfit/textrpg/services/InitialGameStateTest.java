package ru.nsu.ccfit.textrpg.services;


import org.junit.jupiter.api.Test;
import ru.nsu.ccfit.textrpg.TextRpgApplicationTests;
import ru.nsu.ccfit.textrpg.data.entities.ActionType;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class InitialGameStateTest extends TextRpgApplicationTests {

    @Test
    void firstStateTextTest() {
        assertNotNull(state.getText());
        assertEquals("Приветствуем тебя в тестовой комнате", state.getText());
    }

    @Test
    void firstStatePlayerPresentTest() {
        assertNotNull(state.getPlayer());
    }

    @Test
    void firstStatePlayerNameTest() {
        assertEquals("Марио", state.getPlayer().getName());
    }

    @Test
    void firstStatePlayerHpTest() {
        assertEquals(100, state.getPlayer().getHp());
    }


    @Test
    void firstStatePlayerPowerTest() {
        assertEquals(25, state.getPlayer().getPower());
    }

    @Test
    void firstStatePlayerIdTest() {
        assertEquals(4L, state.getPlayer().getId());
    }

    @Test
    void firstStateEnemiesCountTest() {
        assertEquals(0, state.getEnemies().size());
    }

    @Test
    void firstStateEnemiesPresentTest() {
        assertNotNull(state.getEnemies());
    }
    
    @Test
    void firstStateActionsTest() {
        assertNotNull(state.getActions());
        assertEquals(1, state.getActions().length);
        assertEquals(ActionType.CONTINUE, state.getActions()[0]);
    }

    @Test
    void firstStateEnemiesNotNull() {
        assertNotNull(state.getEnemies());
    }

    @Test
    void firstStateEnemiesEmpty() {
        assertEquals(0, state.getEnemies().size());
    }
}