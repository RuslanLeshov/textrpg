package ru.nsu.ccfit.textrpg.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import ru.nsu.ccfit.textrpg.TextRpgApplicationTests;
import ru.nsu.ccfit.textrpg.data.entities.Player;
import ru.nsu.ccfit.textrpg.data.entities.PlayerMock;
import ru.nsu.ccfit.textrpg.data.entities.Stat;
import ru.nsu.ccfit.textrpg.data.repositories.StatRepository;
import ru.nsu.ccfit.textrpg.views.PlayerDto;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

public class PlayerServiceTest extends TextRpgApplicationTests {
    @Mock
    StatRepository statRepository;

    ArrayList<Stat> stats = new ArrayList<>();
    Player player;
    Player savedPlayer;

    PlayerService playerService;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();

        player = new PlayerMock(4L).getPlayer();

        Mockito.doAnswer(invocationOnMock -> {
            stats.add(invocationOnMock.getArgument(0));
            return stats.get(0);
        }).when(statRepository).save(any());

        Mockito.doAnswer(invocationOnMock -> {
            savedPlayer = invocationOnMock.getArgument(0);
            savedPlayer.setId(4L);
            return savedPlayer;
        })
                .when(playerRepository).save(any());

        playerService = new PlayerService(playerRepository, statRepository);
    }

    @Test
    public void testCreatePlayer() {

        PlayerDto playerDto = new PlayerDto(this.player);
        playerDto.setId(null);
        playerService.createPlayer(playerDto);

        assertNotNull(savedPlayer.getId());

        assertEquals("Марио", savedPlayer.getName());

        assertNotNull(savedPlayer.getStats());

        assertEquals(2, savedPlayer.getStats().size());
    }
}
