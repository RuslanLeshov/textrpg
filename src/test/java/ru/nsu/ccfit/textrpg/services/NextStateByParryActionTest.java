package ru.nsu.ccfit.textrpg.services;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.nsu.ccfit.textrpg.TextRpgApplicationTests;
import ru.nsu.ccfit.textrpg.data.entities.ActionType;
import ru.nsu.ccfit.textrpg.domain.EnemyState;
import ru.nsu.ccfit.textrpg.domain.GameState;
import ru.nsu.ccfit.textrpg.views.PlayerAction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class NextStateByParryActionTest extends TextRpgApplicationTests {
    private GameState beforeParryState;

    @BeforeEach
    public void setUp() {
        super.setUp();
        PlayerAction action = new PlayerAction("CONTINUE", null);

        state = game.nextState(action);
        state = game.nextState(action);

        action.setChosenAction("ENTER_BATTLE");
        beforeParryState = game.nextState(action);

        action.setChosenAction("PARRY");
        state = game.nextState(action);
    }


    @Test
    public void stateTextPresent() {
        assertNotNull(state.getText());
    }

    @Test
    public void statePlayerPresent() {
        assertNotNull(state.getPlayer());
    }

    @Test
    public void statePlayerNameNotChanged() {
        assertEquals(beforeParryState.getPlayer().getName(), state.getPlayer().getName());
    }

    @Test
    public void statePlayerHpChanged() {
        int dmg = 22 / 5;
        assertEquals(100 - dmg, state.getPlayer().getHp());
    }

    @Test
    public void statePlayerPowerNotChanged() {
        assertEquals(beforeParryState.getPlayer().getPower(), state.getPlayer().getPower());
    }

    @Test
    public void statePlayerIdNotChanged() {
        assertEquals(beforeParryState.getPlayer().getId(), state.getPlayer().getId());
    }

    @Test
    public void stateEnemiesPresent() {
        assertNotNull(state.getEnemies());
    }

    @Test
    public void stateEnemiesCount() {
        assertEquals(3, state.getEnemies().size());
    }

    @Test
    public void stateActionsPresent() {
        assertNotNull(state.getActions());
    }

    @Test
    public void stateActionsLength() {
        assertEquals(2, state.getActions().length);
    }

    @Test
    public void stateActionsContent() {
        assertEquals(ActionType.ATTACK, state.getActions()[0]);
        assertEquals(ActionType.PARRY, state.getActions()[1]);
    }

    @Test
    public void firstEnemyHurt() {
        EnemyState e = state.getEnemies().get(0);
        int hp = 30 - 25 / 3;
        assertEquals(hp, e.getHp());
    }
}
