package ru.nsu.ccfit.textrpg.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.nsu.ccfit.textrpg.TextRpgApplicationTests;
import ru.nsu.ccfit.textrpg.data.entities.ActionType;
import ru.nsu.ccfit.textrpg.domain.GameState;
import ru.nsu.ccfit.textrpg.views.PlayerAction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class NextStateByContinueActionBeforeBattleTest extends TextRpgApplicationTests {
    private GameState initialState;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        initialState = state;

        PlayerAction action = new PlayerAction("CONTINUE", null);
        state = game.nextState(action);
    }


    @Test
    public void stateTextPresent() {
        assertNotNull(state.getText());
    }

    @Test
    public void stateTextContent() {
        assertEquals("Перед тобой стоит 3 противника", state.getText());
    }

    @Test
    public void statePlayerPresent() {
        assertNotNull(state.getPlayer());
    }

    @Test
    public void statePlayerNameNotChanged() {
        assertEquals(initialState.getPlayer().getName(), state.getPlayer().getName());
    }

    @Test
    public void statePlayerHpNotChanged() {
        assertEquals(initialState.getPlayer().getHp(), state.getPlayer().getHp());
    }

    @Test
    public void statePlayerPowerNotChanged() {
        assertEquals(initialState.getPlayer().getPower(), state.getPlayer().getPower());
    }

    @Test
    public void statePlayerIdNotChanged() {
        assertEquals(initialState.getPlayer().getId(), state.getPlayer().getId());
    }

    @Test
    public void stateEnemiesPresent() {
        assertNotNull(state.getEnemies());
    }

    @Test
    public void stateEnemiesEmpty() {
        assertEquals(0, state.getEnemies().size());
    }

    @Test
    public void stateActionsPresent() {
        assertNotNull(state.getActions());
    }

    @Test
    public void stateActionsLength() {
        assertEquals(1, state.getActions().length);
    }

    @Test
    public void stateActionsContent() {
        assertEquals(ActionType.CONTINUE, state.getActions()[0]);
    }
}
