package ru.nsu.ccfit.textrpg.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.nsu.ccfit.textrpg.TextRpgApplicationTests;
import ru.nsu.ccfit.textrpg.data.entities.ActionType;
import ru.nsu.ccfit.textrpg.domain.EnemyState;
import ru.nsu.ccfit.textrpg.domain.GameState;
import ru.nsu.ccfit.textrpg.views.PlayerAction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class NextStateByAttackActionTest extends TextRpgApplicationTests {
    private GameState beforeAttackState;

    @BeforeEach
    public void setUp() {
        super.setUp();
        PlayerAction action = new PlayerAction("CONTINUE", null);

        state = game.nextState(action);
        state = game.nextState(action);

        action.setChosenAction("ENTER_BATTLE");
        beforeAttackState = game.nextState(action);

        action.setChosenAction("ATTACK");
        action.setActionObject(1L);
        state = game.nextState(action);
    }

    @Test
    public void stateTextPresent() {
        assertNotNull(state.getText());
    }

    @Test
    public void statePlayerPresent() {
        assertNotNull(state.getPlayer());
    }

    @Test
    public void statePlayerNameNotChanged() {
        assertEquals(beforeAttackState.getPlayer().getName(), state.getPlayer().getName());
    }

    @Test
    public void statePlayerHpChanged() {
        assertEquals(78, state.getPlayer().getHp());
    }

    @Test
    public void statePlayerPowerNotChanged() {
        assertEquals(beforeAttackState.getPlayer().getPower(), state.getPlayer().getPower());
    }

    @Test
    public void statePlayerIdNotChanged() {
        assertEquals(beforeAttackState.getPlayer().getId(), state.getPlayer().getId());
    }

    @Test
    public void stateEnemiesPresent() {
        assertNotNull(state.getEnemies());
    }

    @Test
    public void stateEnemiesCount() {
        assertEquals(3, state.getEnemies().size());
    }

    @Test
    public void stateActionsPresent() {
        assertNotNull(state.getActions());
    }

    @Test
    public void stateActionsLength() {
        assertEquals(2, state.getActions().length);
    }

    @Test
    public void stateActionsContent() {
        assertEquals(ActionType.ATTACK, state.getActions()[0]);
        assertEquals(ActionType.PARRY, state.getActions()[1]);
    }

    @Test
    public void firstEnemyHurt() {
        assertEquals(5, state.getEnemies().get(0).getHp());
    }
}
