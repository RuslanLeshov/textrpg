package ru.nsu.ccfit.textrpg.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.nsu.ccfit.textrpg.TextRpgApplicationTests;
import ru.nsu.ccfit.textrpg.data.entities.ActionType;
import ru.nsu.ccfit.textrpg.views.PlayerAction;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class NextStateByFinishBattleActionTest extends TextRpgApplicationTests {

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        PlayerAction action = new PlayerAction("CONTINUE", null);

        state = game.nextState(action);
        state = game.nextState(action);

        action.setChosenAction("ENTER_BATTLE");
        state = game.nextState(action);

        action.setChosenAction("ATTACK");
        action.setActionObject(1L);
        state = game.nextState(action);

        action.setChosenAction("PARRY");
        action.setActionObject(null);
        state = game.nextState(action);

        action.setChosenAction("ATTACK");
        action.setActionObject(2L);
        state = game.nextState(action);

        action.setChosenAction("ATTACK");
        action.setActionObject(2L);
        state = game.nextState(action);

        action.setChosenAction("ATTACK");
        action.setActionObject(3L);
        state = game.nextState(action);

        action.setChosenAction("ATTACK");
        action.setActionObject(3L);
        state = game.nextState(action);

        action.setChosenAction("FINISH_BATTLE");
        action.setActionObject(null);
        state = game.nextState(action);
    }


    @Test
    public void actionsCount() {
        assertEquals(1, state.getActions().length);
    }

    @Test
    public void actionsContent() {
        assertEquals(ActionType.CONTINUE, state.getActions()[0]);
    }
}
