package ru.nsu.ccfit.textrpg;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import ru.nsu.ccfit.textrpg.data.entities.EnemyMock;
import ru.nsu.ccfit.textrpg.data.entities.PlayerMock;
import ru.nsu.ccfit.textrpg.data.entities.Room;
import ru.nsu.ccfit.textrpg.data.entities.RoomMock;
import ru.nsu.ccfit.textrpg.data.repositories.EnemyRepository;
import ru.nsu.ccfit.textrpg.data.repositories.PlayerRepository;
import ru.nsu.ccfit.textrpg.data.repositories.RoomRepository;
import ru.nsu.ccfit.textrpg.domain.GameState;
import ru.nsu.ccfit.textrpg.services.Game;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyLong;

public class TextRpgApplicationTests {
    protected GameState state;

    @Mock
    protected RoomRepository roomRepository;

    @Mock
    protected EnemyRepository enemyRepository;

    @Mock
    protected PlayerRepository playerRepository;

    protected Game game;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        Room roomMock = new RoomMock().getRoom();
        Mockito.when(roomRepository.getOne(1L)).thenReturn(roomMock);
        Mockito.doAnswer(invocationOnMock -> Optional.of(new EnemyMock(invocationOnMock.getArgument(0)).getEnemy()))
                .when(enemyRepository).findById(anyLong());

        Mockito.when(playerRepository.findById(4L)).thenReturn(Optional.of(new PlayerMock(4L).getPlayer()));

        game = new Game(roomRepository, playerRepository, enemyRepository);
        game.setPlayer(4L);
        state = game.initialState();
    }
}
