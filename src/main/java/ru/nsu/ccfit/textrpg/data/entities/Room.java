package ru.nsu.ccfit.textrpg.data.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class Room {
    @Id
    @GeneratedValue
    private Long id;

    @OneToMany
    private List<TextMessage> messagesBeforeBattle;

    @OneToMany
    private List<TextMessage> messagesAfterBattle;

    @OneToMany
    @JoinColumn(name = "room_id")
    private List<Enemy> enemies;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<TextMessage> getMessagesBeforeBattle() {
        return messagesBeforeBattle;
    }

    public void setMessagesBeforeBattle(List<TextMessage> messagesBeforeBattle) {
        this.messagesBeforeBattle = messagesBeforeBattle;
    }

    public List<TextMessage> getMessagesAfterBattle() {
        return messagesAfterBattle;
    }

    public void setMessagesAfterBattle(List<TextMessage> messagesAfterBattle) {
        this.messagesAfterBattle = messagesAfterBattle;
    }

    public List<Enemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<Enemy> enemies) {
        this.enemies = enemies;
    }
}
