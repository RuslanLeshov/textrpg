package ru.nsu.ccfit.textrpg.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.textrpg.data.entities.TextMessage;

public interface TextMessageRepository extends JpaRepository<TextMessage, Long> {
}
