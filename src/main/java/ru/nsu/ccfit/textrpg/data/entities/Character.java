package ru.nsu.ccfit.textrpg.data.entities;

import javax.persistence.*;
import java.util.Map;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Character {
    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @OneToMany
    @JoinColumn(name = "entity_id")
    private Map<String, Stat> stats;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Stat> getStats() {
        return stats;
    }

    public void setStats(Map<String, Stat> stats) {
        this.stats = stats;
    }
}
