package ru.nsu.ccfit.textrpg.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.textrpg.data.entities.Player;

public interface PlayerRepository extends JpaRepository<Player, Long> {

}
