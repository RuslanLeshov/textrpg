package ru.nsu.ccfit.textrpg.data.entities;

public enum ActionType {
    CONTINUE,
    ENTER_BATTLE,
    FINISH_BATTLE,
    EXIT_ROOM,
    ATTACK,
    PARRY
}
