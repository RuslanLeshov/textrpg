package ru.nsu.ccfit.textrpg.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.textrpg.data.entities.Stat;

public interface StatRepository extends JpaRepository<Stat, Long> {

}
