package ru.nsu.ccfit.textrpg.data.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class Stat {
    @Id
    @GeneratedValue
    private Long id;

    private int baseValue;
    private int maxValue;
    private int minValue;

    @OneToMany
    @JoinColumn(name = "stat_id")
    private List<StatModifier> modifiers;

    public Stat() {
    }

    public double calculateValue() {
        double val = baseValue;
        for (StatModifier modifier : modifiers) {
            if (modifier.getType() == StatModifier.Type.MULTIPLIER) {
                val *= modifier.getValue();
            } else if (modifier.getType() == StatModifier.Type.ADDENDUM) {
                val += modifier.getValue();
            }
        }

        if (maxValue >= 0 && val > maxValue) return maxValue;
        if (minValue >= 0 && val < minValue) return minValue;
        return val;
    }

    public void changeBy(double value) {
        StatModifier modifier = new StatModifier();
        modifier.setValue(value);
        modifier.setType(StatModifier.Type.ADDENDUM);
        modifiers.add(modifier);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getBaseValue() {
        return baseValue;
    }

    public void setBaseValue(int baseValue) {
        this.baseValue = baseValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public List<StatModifier> getModifiers() {
        return modifiers;
    }

    public void setModifiers(List<StatModifier> modifiers) {
        this.modifiers = modifiers;
    }
}
