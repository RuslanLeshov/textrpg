package ru.nsu.ccfit.textrpg.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.textrpg.data.entities.Room;

public interface RoomRepository extends JpaRepository<Room, Long> {
}
