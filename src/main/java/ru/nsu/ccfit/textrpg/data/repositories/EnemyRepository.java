package ru.nsu.ccfit.textrpg.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.ccfit.textrpg.data.entities.Enemy;

public interface EnemyRepository extends JpaRepository<Enemy, Long> {
}
