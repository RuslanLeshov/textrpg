package ru.nsu.ccfit.textrpg.services;

import org.springframework.stereotype.Service;
import ru.nsu.ccfit.textrpg.data.entities.Player;
import ru.nsu.ccfit.textrpg.data.entities.Stat;
import ru.nsu.ccfit.textrpg.data.repositories.PlayerRepository;
import ru.nsu.ccfit.textrpg.data.repositories.StatRepository;
import ru.nsu.ccfit.textrpg.views.PlayerDto;

import java.util.ArrayList;
import java.util.HashMap;

@Service
public class PlayerService {
    private final PlayerRepository repository;
    private final StatRepository statRepository;

    public PlayerService(PlayerRepository repository, StatRepository statRepository) {
        this.repository = repository;
        this.statRepository = statRepository;
    }

    public Player createPlayer(PlayerDto player) {
        Player playerEntity = new Player();
        playerEntity.setName(player.getName());
        HashMap <String, Stat> stats = new HashMap<>();

        Stat hp = new Stat();
        hp.setBaseValue(player.getHp());
        hp.setMinValue(0);
        hp.setMaxValue(-1);
        hp.setModifiers(new ArrayList<>());

        Stat power = new Stat();
        power.setBaseValue(player.getPower());
        power.setMinValue(0);
        power.setMaxValue(-1);
        power.setModifiers(new ArrayList<>());


        stats.put("hp", statRepository.save(hp));
        stats.put("power", statRepository.save(power));
        playerEntity.setStats(stats);

        return repository.save(playerEntity);
    }
}
