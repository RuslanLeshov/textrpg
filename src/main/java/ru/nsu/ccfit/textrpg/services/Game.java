package ru.nsu.ccfit.textrpg.services;

import org.springframework.stereotype.Service;
import ru.nsu.ccfit.textrpg.data.entities.*;
import ru.nsu.ccfit.textrpg.data.repositories.EnemyRepository;
import ru.nsu.ccfit.textrpg.data.repositories.PlayerRepository;
import ru.nsu.ccfit.textrpg.data.repositories.RoomRepository;
import ru.nsu.ccfit.textrpg.domain.EnemyState;
import ru.nsu.ccfit.textrpg.domain.GameState;
import ru.nsu.ccfit.textrpg.domain.PlayerState;
import ru.nsu.ccfit.textrpg.views.PlayerAction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class Game {
    private final RoomRepository roomRepository;
    private final PlayerRepository playerRepository;
    private final EnemyRepository enemyRepository;

    private Room room;
    private GameState state;

    private int msgNo;

    public Game(RoomRepository roomRepository,
                PlayerRepository playerRepository,
                EnemyRepository enemyRepository) {
        this.roomRepository = roomRepository;
        this.room = roomRepository.getOne(1L);
        this.playerRepository = playerRepository;
        this.enemyRepository = enemyRepository;

        msgNo = 0;
    }

    public GameState nextState(PlayerAction playerAction) {
        ActionType action = ActionType.valueOf(playerAction.getChosenAction());
        switch (action) {
            case CONTINUE:
                return stateContinue();
            case ENTER_BATTLE:
                return stateEnterBattle();
            case FINISH_BATTLE:
                return stateFinishBattle();
            case EXIT_ROOM:
                return stateExitRoom();
            case ATTACK:
                return stateAttack(enemyRepository
                        .findById(playerAction.getActionObject())
                        .orElseThrow(RuntimeException::new));
            case PARRY:
                return stateParry();
        }
        throw new RuntimeException("Unknown user action");
    }

    private GameState stateParry() {
        state.setText("[Идет битва].");

        List<EnemyState> enemies = state.getEnemies();
        state.setEnemies(new ArrayList<>());
        double dmg = 0;

        PlayerState player = state.getPlayer();

        for (EnemyState e : enemies) {
            e.changeHpBy((int) (-player.getPower() / 3));

            state.setText(state.getText() +
                    " \r\nВы наносите " +
                    (int) (player.getPower() / 3) +
                    " урона врагу '" +
                    e.getName() + "'"
            );

            if (e.getHp() > 0) {
                dmg += e.getPower();
                state.getEnemies().add(e);
            } else {
                state.setText(state.getText() +
                        " \r\n" + e.getName() + " повержен."
                );
            }
        }

        player.changeHpBy((int) (-dmg / 5));
        if (dmg != 0) {
            state.setText(state.getText() + " \r\nПротивники наносят вам " + (int) (dmg / 5) + " урона");
        }
        return updatePlayerAfterFight(player);
    }

    private GameState updatePlayerAfterFight(PlayerState player) {
        state.setPlayer(player);
        if (player.getHp() > 0) {
            state.setActions(new ActionType[]{ActionType.ATTACK, ActionType.PARRY});
        } else {
            state.setText(state.getText() + " \r\nВы проиграли и с позором выгнаны из комнаты");
            state.setActions(new ActionType[]{ActionType.EXIT_ROOM});
        }

        if (state.getEnemies().isEmpty()) {
            state.setText(state.getText() + " \r\nПобеда. Но какой ценой?");
            state.setActions(new ActionType[]{ActionType.FINISH_BATTLE});
        }

        return state;
    }

    private GameState stateAttack(Enemy enemy) {

        state.setText("[Идет битва]");
        PlayerState player = state.getPlayer();

        List<EnemyState> enemies = state.getEnemies();
        state.setEnemies(new ArrayList<>());
        double dmg = 0;
        for (EnemyState e : enemies) {
            if (e.getId().equals(enemy.getId())) {
                e.changeHpBy((int) -player.getPower());

                state.setText(state.getText() +
                        " \r\nВы наносите " +
                        (int) player.getPower() +
                        " урона врагу '" +
                        e.getName() + "'"
                );
            }

            if (e.getHp() > 0) {
                dmg += e.getPower();
                state.getEnemies().add(e);
            } else {
                state.setText(state.getText() +
                        " \r\n" + e.getName() + " повержен."
                );
            }
        }

        player.changeHpBy(-dmg);
        if (dmg != 0) {
            state.setText(state.getText() + " \r\nПротивники наносят вам " + (int) dmg + " урона");
        }
        return updatePlayerAfterFight(player);
    }

    // TODO: переход в новую комнату
    private GameState stateExitRoom() {
        return state;
    }

    private GameState stateFinishBattle() {

        state.setEnemies(Collections.emptyList());

        List<TextMessage> messages = room.getMessagesAfterBattle();
        TextMessage msg = messages.get(0);
        ++msgNo;

        state.setText(msg.getText());
        int sz = room.getMessagesBeforeBattle().size();
        ActionType action;
        if (msgNo - sz == messages.size()) {
            action = ActionType.EXIT_ROOM;
        } else {
            action = ActionType.CONTINUE;
        }

        state.setActions(new ActionType[]{action});

        return state;
    }

    private GameState stateEnterBattle() {
        state.setEnemies(room.getEnemies().stream()
                .map(e -> enemyRepository.findById(e.getId()).orElseThrow(RuntimeException::new))
                .map(EnemyState::new)
                .collect(Collectors.toList())
        );

        state.setActions(new ActionType[]{ActionType.ATTACK, ActionType.PARRY});
        state.setText("[Начался бой]" +
                " \r\nВаши противники:"
        );
        for (EnemyState e : state.getEnemies()) {
            state.setText(state.getText() + " \r\n" + e.getName());
        }

        return state;
    }

    private GameState stateContinue() {
        int sz = room.getMessagesBeforeBattle().size();
        TextMessage msg;
        if (msgNo < sz) {
            List<TextMessage> messages = room.getMessagesBeforeBattle();
            msg = messages.get(msgNo);
        } else {
            List<TextMessage> messages = room.getMessagesAfterBattle();
            msg = messages.get(msgNo - sz);
        }

        state.setText(msg.getText());
        ++msgNo;

        ActionType action;
        if (msgNo == room.getMessagesBeforeBattle().size()) {
            action = ActionType.ENTER_BATTLE;
        } else if (msgNo - sz == room.getMessagesAfterBattle().size()) {
            action = ActionType.EXIT_ROOM;
        } else {
            action = ActionType.CONTINUE;
        }

        state.setActions(new ActionType[]{action});

        return state;
    }

    public GameState initialState() {
        if (state == null) {
            throw new RuntimeException("Player hasn't been set");
        }
        List<TextMessage> messages = room.getMessagesBeforeBattle();
        TextMessage msg = messages.get(0);
        state.setText(msg.getText());
        msgNo = 1;

        state.setEnemies(new ArrayList<>());

        // Обновить данные об игроке
        setPlayer(state.getPlayer().getId());

        ActionType action = (msgNo == messages.size() ? ActionType.ENTER_BATTLE : ActionType.CONTINUE);

        state.setActions(new ActionType[]{action});

        return state;
    }

    public void setPlayer(Long playerId) {
        if (state == null) {
            state = new GameState();
        }
        Player player = playerRepository.findById(playerId).orElseThrow(RuntimeException::new);
        state.setPlayer(new PlayerState(player));
    }
}
