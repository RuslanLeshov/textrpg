package ru.nsu.ccfit.textrpg.controllers;

import org.springframework.web.bind.annotation.*;
import ru.nsu.ccfit.textrpg.domain.GameState;
import ru.nsu.ccfit.textrpg.data.entities.Player;
import ru.nsu.ccfit.textrpg.views.PlayerAction;
import ru.nsu.ccfit.textrpg.views.PlayerDto;
import ru.nsu.ccfit.textrpg.services.Game;
import ru.nsu.ccfit.textrpg.services.PlayerService;

@RestController
public class MainController {
    private final Game game;
    private final PlayerService playerService;

    public MainController(Game game, PlayerService playerService) {
        this.game = game;
        this.playerService = playerService;
    }

    /**
     * Получить данные об игроке по его ID
     *
     * @param player - числовой идентификатор игрока
     * @return - метод возвращает объект, содержащий информацию об игроке
     */
    @GetMapping("/player")
    public PlayerDto getPlayer(@RequestParam Player player) {
        return new PlayerDto(player);
    }

    /**
     * Получить состояние игры на основе действия игрока на прошлом ходу
     *
     * @param playerAction - действие игрока на прошлом ходу
     * @return - следующее состояние игры
     */
    @PostMapping("/state")
    public GameState state(@RequestBody(required = false) PlayerAction playerAction) {
        if (playerAction == null || playerAction.getChosenAction() == null) {
            return game.initialState();
        }
        return game.nextState(playerAction);
    }

    /**
     * Создать игрового персонажа
     *
     * @param player  - данные о персонаже
     * @return - созданный игровой персонаж
     */
    @PostMapping("/player")
    public PlayerDto createPlayer(@RequestBody PlayerDto player) {
        Player createdPlayer = playerService.createPlayer(player);

        game.setPlayer(createdPlayer.getId());

        return new PlayerDto(createdPlayer);
    }
}
