package ru.nsu.ccfit.textrpg.views;

import ru.nsu.ccfit.textrpg.data.entities.Player;
import ru.nsu.ccfit.textrpg.domain.PlayerState;

public class PlayerDto {
    private int hp;
    private Long id;
    private String name;
    private int power;

    public PlayerDto() {
    }

    public PlayerDto(PlayerState playerState) {
        hp = playerState.getHp();
        id = playerState.getId();
        name = playerState.getName();
        power = (int)playerState.getPower();
    }

    public PlayerDto(Player player) {
        this(new PlayerState(player));
    }

    public int getHp() {
        return hp;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
