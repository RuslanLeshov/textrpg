package ru.nsu.ccfit.textrpg.views;

public class PlayerAction {
    private String chosenAction;
    private Long actionObject;

    public PlayerAction() {
    }

    public PlayerAction(String chosenAction, Long actionObject) {
        this.chosenAction = chosenAction;
        this.actionObject = actionObject;
    }

    public String getChosenAction() {
        return chosenAction;
    }

    public void setChosenAction(String chosenAction) {
        this.chosenAction = chosenAction;
    }

    public Long getActionObject() {
        return actionObject;
    }

    public void setActionObject(Long actionObject) {
        this.actionObject = actionObject;
    }
}
