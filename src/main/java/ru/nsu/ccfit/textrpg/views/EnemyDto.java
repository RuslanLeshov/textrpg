package ru.nsu.ccfit.textrpg.views;

import ru.nsu.ccfit.textrpg.data.entities.Enemy;
import ru.nsu.ccfit.textrpg.domain.EnemyState;

public class EnemyDto {
    private Long id;
    private int hp;
    private String name;

    public EnemyDto() {
    }

    public EnemyDto(EnemyState enemyState) {
        this.hp = enemyState.getHp();
        this.id = enemyState.getId();
        this.name = enemyState.getName();
    }

    public EnemyDto(Enemy enemy) {
        this(new EnemyState(enemy));
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
