package ru.nsu.ccfit.textrpg.domain;

import ru.nsu.ccfit.textrpg.data.entities.Stat;
import ru.nsu.ccfit.textrpg.data.entities.Character;

import java.util.Map;

public class CharacterState {
    private Long characterId;

    private String name;

    private Map<String, Stat> stats;

    public CharacterState() {
    }

    public CharacterState(Character character) {
        this.stats = character.getStats();
        this.name = character.getName();
        this.characterId = character.getId();
    }

    public int getHp() {
        return (int) stats.get("hp").calculateValue();
    }

    public double getPower() {
        return stats.get("power").calculateValue();
    }

    public void changeHpBy(double hpDelta) {
        stats.get("hp").changeBy((int)hpDelta);
    }

    public Long getId() {
        return characterId;
    }

    public void setId(Long id) {
        this.characterId = id;
    }

    public String getName() {
        return name;
    }

}
