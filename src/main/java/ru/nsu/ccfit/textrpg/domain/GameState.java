package ru.nsu.ccfit.textrpg.domain;

import ru.nsu.ccfit.textrpg.data.entities.ActionType;

import java.util.List;

public class GameState {
    private String text;
    private ActionType[] actions;
    private PlayerState player;
    private List<EnemyState> enemies;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ActionType[] getActions() {
        return actions;
    }

    public void setActions(ActionType[] actions) {
        this.actions = actions;
    }

    public PlayerState getPlayer() {
        return player;
    }

    public void setPlayer(PlayerState player) {
        this.player = player;
    }

    public List<EnemyState> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<EnemyState> enemies) {
        this.enemies = enemies;
    }
}
