package ru.nsu.ccfit.textrpg.domain;

import ru.nsu.ccfit.textrpg.data.entities.Player;

public class PlayerState extends CharacterState {

    public PlayerState() {
        super();
    }

    public PlayerState(Player player) {
        super(player);
    }
}
