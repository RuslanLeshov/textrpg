package ru.nsu.ccfit.textrpg.domain;

import ru.nsu.ccfit.textrpg.data.entities.Enemy;

public class EnemyState extends CharacterState {
    public EnemyState() {
        super();
    }
    public EnemyState(Enemy enemy) {
        super(enemy);
    }
}
