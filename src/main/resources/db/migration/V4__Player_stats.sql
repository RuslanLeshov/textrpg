drop table enemy;
drop table player;

create table character
(
    id   int8 not null,
    name varchar(255),
    primary key (id)
);
create table enemy
(
    id      int8 not null,
    room_id int8,
    primary key (id)
);
create table player
(
    id int8 not null,
    primary key (id)
);

create table stat
(
    id         int8 not null,
    entity_id  int8,
    base_value int4 not null,
    max_value  int4 not null,
    min_value  int4 not null,
    stats_key  varchar(255),
    primary key (id)
);
create table stat_modifier
(
    id           int8   not null,
    duration     int4   not null,
    type         int4,
    value        float8 not null,
    stat_id int8,
    primary key (id)
);

alter table if exists stat
    add constraint fk_stat_character_entity_id foreign key (entity_id) references character;
alter table if exists enemy
    add constraint fk_enemy_character_id foreign key (id) references character;
alter table if exists enemy
    add constraint fk_enemy_room_room_id foreign key (room_id) references room;
alter table if exists player
    add constraint fk_player_character_id foreign key (id) references character;
alter table if exists stat_modifier
    add constraint fk_stat_modifier_stat_stat_id foreign key (stat_id) references stat;
