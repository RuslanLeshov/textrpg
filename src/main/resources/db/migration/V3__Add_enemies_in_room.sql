drop table if exists enemy;

create table enemy
(
    id    int8 not null,
    room_id int8 not null,
    hp    int4 not null,
    name  varchar(255),
    power int4 not null,
    primary key (id)
);

alter table if exists enemy
    add constraint fk_enemy_room foreign key (room_id) references room;


insert into enemy(id, room_id, hp, name, power) values (1, 1, 30, 'Дикий кабан', 5);
insert into enemy(id, room_id, hp, name, power) values (2, 1, 50, 'Каменный страж', 10);
insert into enemy(id, room_id, hp, name, power) values (3, 1, 40, 'Гоблин из подземелья', 7);
