drop sequence if exists hibernate_sequence;

-- Enemies in first room
insert into character(id, name) values (1, 'Дикий кабан');
insert into character(id, name) values (2, 'Каменный страж');
insert into character(id, name) values (3, 'Гоблин из подземелья');

insert into enemy(id, room_id) values (1, 1);
insert into enemy(id, room_id) values (2, 1);
insert into enemy(id, room_id) values (3, 1);


-- Player
insert into character (id, name) values (4, 'Марио');
insert into player (id) values (4);


-- Enemy stats
insert into stat (id, entity_id, base_value, max_value, min_value, stats_key)
values (1, 1, 30, -1, 0, 'hp');

insert into stat (id, entity_id, base_value, max_value, min_value, stats_key)
values (2, 1, 5, -1, -1, 'power');

insert into stat (id, entity_id, base_value, max_value, min_value, stats_key)
values (3, 2, 50, -1, 0, 'hp');

insert into stat (id, entity_id, base_value, max_value, min_value, stats_key)
values (4, 2, 10, -1, -1, 'power');

insert into stat (id, entity_id, base_value, max_value, min_value, stats_key)
values (5, 3, 40, -1, 0, 'hp');

insert into stat (id, entity_id, base_value, max_value, min_value, stats_key)
values (6, 3, 7, -1, -1, 'power');


-- Player stats
insert into stat (id, entity_id, base_value, max_value, min_value, stats_key)
values (7, 4, 100, -1, 0, 'hp');

insert into stat (id, entity_id, base_value, max_value, min_value, stats_key)
values (8, 4, 25, -1, -1, 'power');


-- Hibernate sequence
create sequence hibernate_sequence start 9 increment 1;