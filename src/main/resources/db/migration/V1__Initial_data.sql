create table enemy
(
    id    int8 not null,
    hp    int4 not null,
    name  varchar(255),
    power int4 not null,
    primary key (id)
);

create table player
(
    id    int8 not null,
    hp    int4 not null,
    name  varchar(255),
    power int4 not null,
    primary key (id)
);


-- Inserts into Enemy

insert into enemy(id, hp, name, power) values (1, 30, 'Дикий кабан', 5);
insert into enemy(id, hp, name, power) values (2, 50, 'Каменный страж', 10);
insert into enemy(id, hp, name, power) values (3, 40, 'Гоблин из подземелья', 7);


-- Inserts into Player

insert into player (id, hp, name, power)
values (1, 100, 'Марио', 25);


-- Hibernate sequence

create sequence hibernate_sequence start 4 increment 1;