create table room
(
    id int8 not null,
    primary key (id)
);
create table room_messages_after_battle
(
    room_id                  int8 not null,
    messages_after_battle_id int8 not null
);
create table room_messages_before_battle
(
    room_id                   int8 not null,
    messages_before_battle_id int8 not null
);
create table text_message
(
    id   int8 not null,
    text varchar(1024),
    primary key (id)
);

alter table if exists room_messages_after_battle
    add constraint uniq_messages_after_battle_id unique (messages_after_battle_id);
alter table if exists room_messages_before_battle
    add constraint uniq_messages_before_battle_id unique (messages_before_battle_id);

alter table if exists room_messages_after_battle
    add constraint fk_room_messages_after_battle_message foreign key (messages_after_battle_id) references text_message;
alter table if exists room_messages_after_battle
    add constraint fk_room_messages_after_battle_room foreign key (room_id) references room;

alter table if exists room_messages_before_battle
    add constraint fk_room_messages_before_battle_message foreign key (messages_before_battle_id) references text_message;
alter table if exists room_messages_before_battle
    add constraint fk_room_messages_before_battle_room foreign key (room_id) references room;


-- Insert

insert into room(id)
values (1);

insert into text_message(id, text)
values (1, 'Приветствуем тебя в тестовой комнате');

insert into text_message(id, text)
values (2, 'Перед тобой стоит 3 противника');

insert into text_message(id, text)
values (3, 'Тебе нужно сразиться с ними');

insert into text_message(id, text)
values (4, 'Первая победа!');

insert into text_message(id, text)
values (5, 'Поздравляю. Теперь ты готов к новым испытаниям');

insert into text_message(id, text)
values (6, 'Удачи, герой');

insert into room_messages_before_battle(room_id, messages_before_battle_id) values (1, 1);
insert into room_messages_before_battle(room_id, messages_before_battle_id) values (1, 2);
insert into room_messages_before_battle(room_id, messages_before_battle_id) values (1, 3);

insert into room_messages_after_battle(room_id, messages_after_battle_id) values (1, 4);
insert into room_messages_after_battle(room_id, messages_after_battle_id) values (1, 5);
insert into room_messages_after_battle(room_id, messages_after_battle_id) values (1, 6);
