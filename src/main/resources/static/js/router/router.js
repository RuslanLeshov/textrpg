import Vue from 'vue'
import VueRouter from 'vue-router'
import MainPage from "../pages/MainPage.vue";
import StartPage from "../pages/StartPage.vue";

Vue.use(VueRouter);

const routes = [
    { path: '/', component: StartPage},
    { path: '/game', component: MainPage, props: (route)=> ({playerId: route.query.playerId})},
    /* { path: '/w/:id', component: WorkersPage},
     { path: '/p', component: ProviderList, props: (route) => ({size: route.query.size, page: route.query.page})},
     { path: '/', component: OutletsList },
     { path: '/prod/:id', component: ProductList}*/
];

export default new VueRouter({
    mode: 'history',
    routes: routes
});